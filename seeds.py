from django_seed import Seed

seeder = Seed.seeder()

from mapi.models import Machines, Hardware
seeder.add_entity(Machines, 5)
# seeder.add_entity(Hardware, 10)

inserted_pks = seeder.execute()