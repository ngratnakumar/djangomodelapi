from django.db import models

class Hardware(models.Model):
    hardwareid = models.AutoField(primary_key=True)
    make = models.CharField(max_length=45)
    model = models.CharField(max_length=45)
    in_waranty = models.BooleanField()
    processor_make = models.CharField(max_length=45, blank=True, null=True)
    sockets = models.IntegerField(blank=True, null=True)
    cores = models.IntegerField(blank=True, null=True)
    processor_speed = models.FloatField(blank=True, null=True)
    generation = models.IntegerField(blank=True, null=True)
    stauts = models.CharField(max_length=45, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    front_img = models.TextField(blank=True, null=True)
    back_img = models.TextField(blank=True, null=True)
    ram_make = models.CharField(max_length=45, blank=True, null=True)
    ram_capacity = models.CharField(max_length=45, blank=True, null=True)
    ram_type = models.CharField(max_length=45, blank=True, null=True)
    is_laptop = models.BooleanField()
    wireless_make_model = models.CharField(max_length=100, blank=True, null=True)
    vendorid = models.ForeignKey('Vendors', models.DO_NOTHING, db_column='vendorid', null=True)
    subscriptionid = models.ForeignKey('Subscriptions', models.DO_NOTHING, db_column='subscriptionid', null=True)

    def __str__(self):
        return self.make+" "+self.model

    class Meta:
        managed = True
        db_table = 'hardware'


class Hdds(models.Model):
    hddid = models.AutoField(primary_key=True)
    make = models.CharField(max_length=20)
    model = models.CharField(max_length=30, blank=True, null=True)
    version = models.CharField(max_length=10, blank=True, null=True)
    speed = models.CharField(max_length=10, blank=True, null=True)
    capacity = models.CharField(max_length=10)
    raid_type = models.CharField(max_length=10, blank=True, null=True)
    raid_hddid = models.IntegerField(blank=True, null=True)
    warrantyid = models.ForeignKey('Warranty', models.DO_NOTHING, db_column='warrantyid', blank=True, null=True)
    serial_number = models.CharField(max_length=100, blank=True, null=True)
    hardwareid = models.ForeignKey(Hardware, models.DO_NOTHING, db_column='hardwareid', null=True)
    vendorid = models.ForeignKey('Vendors', models.DO_NOTHING, db_column='vendorid', blank=True, null=True)

    def __str__(self):
        return self.make

    class Meta:
        managed = True
        db_table = 'hdds'


class Locations(models.Model):
    locationid = models.AutoField(primary_key=True)
    room_number = models.CharField(max_length=30, unique=True, blank=False)
    campus = models.CharField(max_length=45, blank=False)

    def __str__(self):
        return self.room_number

    class Meta:
        managed = True
        db_table = 'locations'


class Machines(models.Model):
    machineid = models.AutoField(primary_key=True)
    hostname = models.CharField(max_length=45)
    accession_number = models.CharField(max_length=10)
    is_ssl = models.BooleanField("SSL Certificate")
    ssl_start = models.DateField(blank=True, null=True)
    ssl_end = models.DateField(blank=True, null=True)
    operating_system = models.CharField(max_length=150, blank=True, null=True)
    os_version = models.FloatField(blank=True, null=True)
    hardwareid = models.ForeignKey(Hardware, models.DO_NOTHING, db_column='hardwareid', null=True)
    locationid = models.ForeignKey(Locations, models.DO_NOTHING, db_column='locationid', null=True)

    def __str__(self):
        return self.hostname

    class Meta:
        managed = True
        db_table = 'machines'


class Network(models.Model):
    networkid = models.AutoField(primary_key=True)
    interface_name = models.CharField(max_length=45)
    mac_address = models.CharField(max_length=50)
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    speed = models.CharField(max_length=10, blank=True, null=True)
    make = models.CharField(max_length=45, blank=True, null=True)
    model = models.CharField(max_length=45, blank=True, null=True)
    is_active = models.BooleanField()
    status = models.CharField(max_length=45, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    machineid = models.ForeignKey(Machines, models.DO_NOTHING, db_column='machineid', null=True)
    switch_port = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'network'


class Subscriptions(models.Model):
    subscriptionid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    type = models.CharField(max_length=45)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    is_active = models.BooleanField()
    contact_person1 = models.CharField(max_length=45, blank=True, null=True)
    contact1 = models.CharField(max_length=20, blank=True, null=True)
    email1 = models.CharField(max_length=50, blank=True, null=True)
    contact_person2 = models.CharField(max_length=45, blank=True, null=True)
    contact2 = models.CharField(max_length=20, blank=True, null=True)
    email2 = models.CharField(max_length=50, blank=True, null=True)
    comapany_name = models.CharField(max_length=50, blank=True, null=True)
    version = models.CharField(max_length=50, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'subscriptions'


class SwitchPorts(models.Model):
    portid = models.AutoField(primary_key=True)
    switchid = models.ForeignKey('Switches', models.DO_NOTHING, db_column='switchid', null=True)
    port_number = models.IntegerField()
    type = models.CharField(max_length=45)
    speed = models.CharField(max_length=16, blank=True, null=True)
    vlan_untagged1 = models.CharField(max_length=45, blank=True, null=True)
    vlan_untagged2 = models.CharField(max_length=45, blank=True, null=True)
    vlan_untagged3 = models.CharField(max_length=45, blank=True, null=True)
    vlan_untagged4 = models.CharField(max_length=45, blank=True, null=True)
    vlan_tagged = models.IntegerField(blank=True, null=True)
    is_assigned = models.IntegerField(blank=True, null=True)
    is_uplink = models.BooleanField()
    is_mac_auth = models.BooleanField()
    is_trunk = models.BooleanField()
    trunk_switch_id = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'switch_ports'


class Switches(models.Model):
    switchid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    locationid = models.ForeignKey(Locations, models.DO_NOTHING, db_column='locationid', null=True)
    type = models.CharField(max_length=45)
    make = models.CharField(max_length=45, blank=True, null=True)
    model = models.CharField(max_length=45, blank=True, null=True)
    serial_number = models.CharField(max_length=150)
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    mac_address = models.CharField(max_length=50)
    number_of_ports = models.IntegerField()
    in_warranty = models.IntegerField(blank=True, null=True)
    web_enabled = models.IntegerField(blank=True, null=True)
    firmware_name = models.CharField(max_length=45, blank=True, null=True)
    firmware_version = models.CharField(max_length=45, blank=True, null=True)
    fw_lpdated_on = models.DateField(blank=True, null=True)
    last_changed = models.DateTimeField(blank=True, null=True)
    vendorid = models.ForeignKey('Vendors', models.DO_NOTHING, db_column='vendorid', blank=True, null=True)
    uplink_from = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'switches'


class Vendors(models.Model):
    vendorid = models.AutoField(primary_key=True)
    vendor_name = models.CharField(max_length=100)
    city = models.CharField(max_length=20)
    address = models.CharField(max_length=100, blank=True, null=True)
    contact1 = models.CharField(max_length=20, blank=True, null=True)
    contact2 = models.CharField(max_length=20, blank=True, null=True)
    contact_person1 = models.CharField(max_length=50, blank=True, null=True)
    contact_person2 = models.CharField(max_length=50, blank=True, null=True)
    email1 = models.CharField(max_length=45, blank=True, null=True)
    email2 = models.CharField(max_length=45, blank=True, null=True)
    email3 = models.CharField(max_length=45, blank=True, null=True)
    details = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=50, blank=True, null=True)
    preferred = models.IntegerField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.vendor_name+", "+self.city

    class Meta:
        managed = True
        db_table = 'vendors'


class Warranty(models.Model):
    warrantyid = models.AutoField(primary_key=True)
    vendorid = models.IntegerField(blank=True, null=True)
    vendor_name = models.CharField(max_length=45, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    is_active = models.BooleanField()
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'warranty'

class Testing(models.Model):
    testid = models.AutoField(primary_key=True)
    test_name = models.CharField(max_length=45, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'testing'
