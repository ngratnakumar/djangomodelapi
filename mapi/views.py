from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from rest_framework import viewsets 

# class hardwareList(APIView):

#     def get(self, requeest):
#         hardware1 = Hardware.objects.all()
#         serializer = DynamicFieldsModelSerializer(hardware1, many=True)
#         return Response(serializer.data)

#     def post(self):
#         pass

class HardwareList(viewsets.ModelViewSet):
    queryset = Hardware.objects.all()
    serializer_class = HardwareSerializer

class HddsList(viewsets.ModelViewSet):
    queryset = Hdds.objects.all()
    serializer_class = HddsSerializer

class LocationsList(viewsets.ModelViewSet):
    queryset = Locations.objects.all()
    serializer_class = LocationsSerializer

class MachinesList(viewsets.ModelViewSet):
    queryset = Machines.objects.all()
    serializer_class = MachinesSerializer

class NetworkList(viewsets.ModelViewSet):
    queryset = Network.objects.all()
    serializer_class = NetworkSerializer

class SubscriptionsList(viewsets.ModelViewSet):
    queryset = Subscriptions.objects.all()
    serializer_class = SubscriptionsSerializer

class SwitchPortsList(viewsets.ModelViewSet):
    queryset = SwitchPorts.objects.all()
    serializer_class = SwitchPortsSerializer

class SwitchesList(viewsets.ModelViewSet):
    queryset = Switches.objects.all()
    serializer_class = SwitchesSerializer

class VendorsList(viewsets.ModelViewSet):
    queryset = Vendors.objects.all()
    serializer_class = VendorsSerializer

class WarrantyList(viewsets.ModelViewSet):
    queryset = Warranty.objects.all()
    serializer_class = WarrantySerializer