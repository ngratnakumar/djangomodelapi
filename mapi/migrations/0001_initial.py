# Generated by Django 3.0.5 on 2020-04-27 07:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hardware',
            fields=[
                ('hardwareid', models.AutoField(primary_key=True, serialize=False)),
                ('make', models.CharField(max_length=45)),
                ('model', models.CharField(max_length=45)),
                ('in_waranty', models.BooleanField()),
                ('processor_make', models.CharField(blank=True, max_length=45, null=True)),
                ('sockets', models.IntegerField(blank=True, null=True)),
                ('cores', models.IntegerField(blank=True, null=True)),
                ('processor_speed', models.FloatField(blank=True, null=True)),
                ('generation', models.IntegerField(blank=True, null=True)),
                ('stauts', models.CharField(blank=True, max_length=45, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('front_img', models.TextField(blank=True, null=True)),
                ('back_img', models.TextField(blank=True, null=True)),
                ('ram_make', models.CharField(blank=True, max_length=45, null=True)),
                ('ram_capacity', models.CharField(blank=True, max_length=45, null=True)),
                ('ram_type', models.CharField(blank=True, max_length=45, null=True)),
                ('is_laptop', models.BooleanField()),
                ('wireless_make_model', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'db_table': 'hardware',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Locations',
            fields=[
                ('locationid', models.AutoField(primary_key=True, serialize=False)),
                ('room_number', models.CharField(max_length=10, unique=True)),
                ('campus', models.CharField(max_length=45)),
            ],
            options={
                'db_table': 'locations',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Machines',
            fields=[
                ('machineid', models.AutoField(primary_key=True, serialize=False)),
                ('hostname', models.CharField(max_length=45)),
                ('accession_number', models.CharField(max_length=10)),
                ('is_ssl', models.BooleanField(verbose_name='SSL Certificate')),
                ('ssl_start', models.DateField(blank=True, null=True)),
                ('ssl_end', models.DateField(blank=True, null=True)),
                ('operating_system', models.CharField(blank=True, max_length=150, null=True)),
                ('os_version', models.FloatField(blank=True, null=True)),
                ('hardwareid', models.ForeignKey(db_column='hardwareid', on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Hardware')),
                ('locationid', models.ForeignKey(db_column='locationid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Locations')),
            ],
            options={
                'db_table': 'machines',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Subscriptions',
            fields=[
                ('subscriptionid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=45)),
                ('type', models.CharField(max_length=45)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('is_active', models.BooleanField()),
                ('contact_person1', models.CharField(blank=True, max_length=45, null=True)),
                ('contact1', models.CharField(blank=True, max_length=20, null=True)),
                ('email1', models.CharField(blank=True, max_length=50, null=True)),
                ('contact_person2', models.CharField(blank=True, max_length=45, null=True)),
                ('contact2', models.CharField(blank=True, max_length=20, null=True)),
                ('email2', models.CharField(blank=True, max_length=50, null=True)),
                ('comapany_name', models.CharField(blank=True, max_length=50, null=True)),
                ('version', models.CharField(blank=True, max_length=50, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'subscriptions',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Switches',
            fields=[
                ('switchid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=45)),
                ('type', models.CharField(max_length=45)),
                ('make', models.CharField(blank=True, max_length=45, null=True)),
                ('model', models.CharField(blank=True, max_length=45, null=True)),
                ('serial_number', models.CharField(max_length=150)),
                ('ip_address', models.CharField(blank=True, max_length=50, null=True)),
                ('mac_address', models.CharField(max_length=50)),
                ('number_of_ports', models.IntegerField()),
                ('in_warranty', models.IntegerField(blank=True, null=True)),
                ('web_enabled', models.IntegerField(blank=True, null=True)),
                ('firmware_name', models.CharField(blank=True, max_length=45, null=True)),
                ('firmware_version', models.CharField(blank=True, max_length=45, null=True)),
                ('fw_lpdated_on', models.DateField(blank=True, null=True)),
                ('last_changed', models.DateTimeField(blank=True, null=True)),
                ('uplink_from', models.IntegerField(blank=True, null=True)),
                ('locationid', models.ForeignKey(db_column='locationid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Locations')),
            ],
            options={
                'db_table': 'switches',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Testing',
            fields=[
                ('testid', models.AutoField(primary_key=True, serialize=False)),
                ('test_name', models.CharField(blank=True, max_length=45, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'testing',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Vendors',
            fields=[
                ('vendorid', models.AutoField(primary_key=True, serialize=False)),
                ('vendor_name', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=20)),
                ('address', models.CharField(blank=True, max_length=100, null=True)),
                ('contact1', models.CharField(blank=True, max_length=20, null=True)),
                ('contact2', models.CharField(blank=True, max_length=20, null=True)),
                ('contact_person1', models.CharField(blank=True, max_length=50, null=True)),
                ('contact_person2', models.CharField(blank=True, max_length=50, null=True)),
                ('email1', models.CharField(blank=True, max_length=45, null=True)),
                ('email2', models.CharField(blank=True, max_length=45, null=True)),
                ('email3', models.CharField(blank=True, max_length=45, null=True)),
                ('details', models.CharField(blank=True, max_length=255, null=True)),
                ('status', models.CharField(blank=True, max_length=50, null=True)),
                ('preferred', models.IntegerField(blank=True, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'vendors',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Warranty',
            fields=[
                ('warrantyid', models.AutoField(primary_key=True, serialize=False)),
                ('vendorid', models.IntegerField(blank=True, null=True)),
                ('vendor_name', models.CharField(blank=True, max_length=45, null=True)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('is_active', models.BooleanField()),
                ('comments', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'warranty',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SwitchPorts',
            fields=[
                ('portid', models.AutoField(primary_key=True, serialize=False)),
                ('port_number', models.IntegerField()),
                ('type', models.CharField(max_length=45)),
                ('speed', models.CharField(blank=True, max_length=16, null=True)),
                ('vlan_untagged1', models.CharField(blank=True, max_length=45, null=True)),
                ('vlan_untagged2', models.CharField(blank=True, max_length=45, null=True)),
                ('vlan_untagged3', models.CharField(blank=True, max_length=45, null=True)),
                ('vlan_untagged4', models.CharField(blank=True, max_length=45, null=True)),
                ('vlan_tagged', models.IntegerField(blank=True, null=True)),
                ('is_assigned', models.IntegerField(blank=True, null=True)),
                ('is_uplink', models.BooleanField()),
                ('is_mac_auth', models.BooleanField()),
                ('is_trunk', models.BooleanField()),
                ('trunk_switch_id', models.IntegerField(blank=True, null=True)),
                ('status', models.CharField(blank=True, max_length=45, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('switchid', models.ForeignKey(db_column='switchid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Switches')),
            ],
            options={
                'db_table': 'switch_ports',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='switches',
            name='vendorid',
            field=models.ForeignKey(blank=True, db_column='vendorid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Vendors'),
        ),
        migrations.CreateModel(
            name='Network',
            fields=[
                ('networkid', models.AutoField(primary_key=True, serialize=False)),
                ('interface_name', models.CharField(max_length=45)),
                ('mac_address', models.CharField(max_length=50)),
                ('ip_address', models.CharField(blank=True, max_length=50, null=True)),
                ('speed', models.CharField(blank=True, max_length=10, null=True)),
                ('make', models.CharField(blank=True, max_length=45, null=True)),
                ('model', models.CharField(blank=True, max_length=45, null=True)),
                ('is_active', models.BooleanField()),
                ('status', models.CharField(blank=True, max_length=45, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('switch_port', models.IntegerField(blank=True, null=True)),
                ('machineid', models.ForeignKey(db_column='machineid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Machines')),
            ],
            options={
                'db_table': 'network',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Hdds',
            fields=[
                ('hddid', models.AutoField(primary_key=True, serialize=False)),
                ('make', models.CharField(max_length=20)),
                ('model', models.CharField(blank=True, max_length=30, null=True)),
                ('version', models.CharField(blank=True, max_length=10, null=True)),
                ('speed', models.CharField(blank=True, max_length=10, null=True)),
                ('capacity', models.CharField(max_length=10)),
                ('raid_type', models.CharField(blank=True, max_length=10, null=True)),
                ('raid_hddid', models.IntegerField(blank=True, null=True)),
                ('serial_number', models.CharField(blank=True, max_length=100, null=True)),
                ('hardwareid', models.ForeignKey(db_column='hardwareid', on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Hardware')),
                ('vendorid', models.ForeignKey(blank=True, db_column='vendorid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Vendors')),
                ('warrantyid', models.ForeignKey(blank=True, db_column='warrantyid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Warranty')),
            ],
            options={
                'db_table': 'hdds',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='hardware',
            name='subscriptionid',
            field=models.ForeignKey(blank=True, db_column='subscriptionid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Subscriptions'),
        ),
        migrations.AddField(
            model_name='hardware',
            name='vendorid',
            field=models.ForeignKey(blank=True, db_column='vendorid', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapi.Vendors'),
        ),
    ]
