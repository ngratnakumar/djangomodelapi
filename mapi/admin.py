from django.contrib import admin
from mapi.models import *
# from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter, ChoiceDropdownFilter
# from django_reverse_admin import ReverseModelAdmin

class LocationsDisplay(admin.ModelAdmin):
    list_display=('room_number', 'campus')
    list_filter=('room_number',)


class MachinesDisplay(admin.ModelAdmin):
    # list_display=[field.name for field in Machines._meta.get_fields()]
    list_display = ('hostname', 'make_model', 'location', 'accession_number', 'networks', 'is_ssl', 'operating_system', 'os_version')
    def make_model(self, obj):
        return obj.hardwareid

    def location(self, obj):
        return obj.locationid.room_number+", "+obj.locationid.campus
        
    def networks(self, obj):
        network_objects = Network.objects.all().filter(machineid=obj.machineid)
        nids = []
        for each_nid in network_objects:
            nids.append(each_nid.interface_name+"-"+each_nid.ip_address+": "+each_nid.comments)
        return " ,".join(nids)
        list_filter=('hostname',)
    inline_type = 'stacked'

class NetworkDisplay(admin.ModelAdmin):
    list_display = ('hostname', 'ip_address', 'interface_mac', 'comments', 'is_active')

    def hostname(self, obj):
        return obj.machineid.hostname

    def interface_mac(self, obj):
        return obj.interface_name+"-"+obj.mac_address
    list_filter = ('ip_address', )

class HardwareDisplay(admin.ModelAdmin):
    list_display = ('make', 'model', 'in_waranty', 'processor_make','cores','processor_speed','ram_capacity',)

# class VendorDisplay

admin.site.register(Hardware, HardwareDisplay)
admin.site.register(Hdds)
admin.site.register(Locations, LocationsDisplay)
admin.site.register(Machines, MachinesDisplay)
admin.site.register(Network, NetworkDisplay)
admin.site.register(Subscriptions)
admin.site.register(SwitchPorts)
admin.site.register(Switches)
admin.site.register(Vendors)
admin.site.register(Warranty, name="Warranty")

admin.site.site_header = "NCRA Machines Management System"