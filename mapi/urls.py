"""nmms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
# from rest_framework.urlpatterns import format_suffix_patterns
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('mapi/hardware', views.HardwareList)
router.register('mapi/hdds', views.HddsList)
router.register('mapi/locations', views.LocationsList)
router.register('mapi/machines', views.MachinesList)
router.register('mapi/network', views.NetworkList)
router.register('mapi/subscriptions', views.SubscriptionsList)
router.register('mapi/switchports', views.SwitchPortsList)
router.register('mapi/switches', views.SwitchesList)
router.register('mapi/vendors', views.VendorsList)
router.register('mapi/warranty', views.WarrantyList)

urlpatterns = [
    path('', include(router.urls))
]
