django-admin startproject modeltut

pip install virtualenv Django djangorestframework psycopg2

virtualenv modeltutenv


modeltutenv\Scripts\activate.bat


python manage.py starapp mapi

add mapi to setting.py INSTALLED_APPS



add postgres configuration to DATABASES
replace the sqlite with postgres configuration


sqlite ---
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    } 


postgres ---
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }



Inside mapi/models.py add the nmms_models


python manage.py makemigrations

python manage.py migrate // will create all default django_application tables

python manage.py makemigrations mapi

python manage.py migrate mapi


--- Creating admin interface

(modeltutenv) C:\Users\ngrat\projects\modeltut>python manage.py createsuperuser
Username (leave blank to use 'ngrat'): ratnakumar
Email address: 6nrkum@r191291P
Error: Enter a valid email address.
Email address: ngratnakumar@gmail.com
Password:
Password (again):
Superuser created successfully.
